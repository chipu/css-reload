# CSS-Reload

## Server

```
#!bash

$ composer update
$ pecl install inotify
$ php css-reload.php
```

## Client 

```
#!html
<script src='css-reload.js?root=/var/www/sitename'></script> 
```