
if typeof reload_css_config == "undefined"
  reload_css_config =
    root: '/var/www/html'
    endpoint: "ws://localhost:33333"

scripts = document.getElementsByTagName('script');
script = scripts[scripts.length - 1];
qs = script.src.replace(/^[^\?]+\??/,'');

for item in qs.split('&')
  pair = item.split('=')
  key = decodeURIComponent(pair[0])
  value = decodeURIComponent(pair[1])
  reload_css_config[key] = value

console.log reload_css_config

get_stylesheets = () ->
  i = 0
  result = []
  while link = document.getElementsByTagName("link")[i++]
    if link.rel and "stylesheet" == link.rel.toLowerCase()
      if not link._href
        link._href = link.href
      result.push(link)
  return result

chrome_fix = () ->
  document.getElementsByTagName('body')[0].focus()

reload_css = () ->
  console.log "Reloading CSS"
  for link in get_stylesheets()
    s = if link._href.indexOf("?") > -1 then "&" else "?"
    link.href = link._href + s + (new Date).getTime()
  chrome_fix()
  setTimeout chrome_fix, 500

try_connect = () ->
  try
    socket = new WebSocket(reload_css_config.endpoint)
    socket.onopen = (event) ->
      console.log "Reload CSS Ready"
      data =
        action: "watch"
        root: reload_css_config.root
        files: []
      for link in get_stylesheets()
        data.files.push(link.href)
      if data.files.length > 0
        this.send(JSON.stringify(data))

    socket.onclose = (event) ->
      try_connect()

    socket.onmessage = (event) ->
      console.log "Reloading CSS"
      data = JSON.parse(event.data)
      if data.action == 'reload'
        reload_css()
  catch e
    console.log e
    setTimeout try_connect, 5000
    return false

try_connect()
