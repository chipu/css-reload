<?php

require 'vendor/autoload.php';

use Ratchet\Server\IoServer;
use Ratchet\MessageComponentInterface;
use Ratchet\Http\HttpServer;
use Ratchet\WebSocket\WsServer;
use Ratchet\ConnectionInterface;
use React\EventLoop\Factory as LoopFactory;
use React\Socket\Server as Reactor;

function debug($format) {
    if(!is_string($format)) {
        echo print_r($format, true);
    } else {
        echo call_user_func_array('sprintf', func_get_args());
    }
    echo PHP_EOL;
}

class Container implements JsonSerializable {
    public $properties;

    function __construct(array $properties = []) {
        $this->properties = array_merge(static::properties(), $properties);
        $this->init();
    }

    function &__get($key) {
        return $this->properties[$key];
    }

    function __set($key, $value) {
        $this->properties[$key] = $value;
    }

    function init() {
    }

    function properties() {
        return [];
    }

    function jsonSerialize() {
        return $this->properties;
    }

    static function create() {
        $args = func_get_args();
        $properties = static::properties();
        foreach($properties as $key => $value) {
            if(empty($args)) {
                break;
            }
            $properties[$key] = array_shift($args);
        }
        return new static($properties);
    }
}

class Client extends Container {
    function properties() {
        return [
            'id' => uniqid(),
            'class' => get_class($this),
            'root' => null,
            'files' => [],
            'watches' => [],
        ];
    }

    function onUpdate($now, $delta, $events) {
        if($events) {
            debug("YEEAH");
            debug($events);
            // TODO: Check if file is actually used by this client
            $this->connection->send(json_encode([
                'action' => 'reload',
            ]));
        }
    }

    function onMessage(ConnectionInterface $from, $msg) {
        switch($msg->action) {
        case 'watch':
            $this->clearWatches();
            $this->root = $msg->root;
            $this->files = $msg->files;
            foreach($this->files as $file) {
                $pathname = $this->root . parse_url($file, PHP_URL_PATH);
                if(is_file($pathname)) {
                    $this->watches[] = inotify_add_watch($this->server->inotify, $pathname, IN_MODIFY);
                    debug("watch: $file => $pathname");
                } else {
                    debug("file not found: %s", $pathname);
                }
            }
            break;
        }
    }

    function onClose() {
        $this->clearWatches();
    }

    function clearWatches() {
        foreach($this->watches as $watch) {
            inotify_rm_watch($this->server->inotify, $watch);
        }
        $this->watches = [];
    }
}

class Server implements MessageComponentInterface {
    protected $connections;
    protected $lastUpdate = null;
    public $inotify;

    function __construct() {
        debug(__METHOD__);
        $this->connections = new \SplObjectStorage;
        $this->inotify = inotify_init();
        stream_set_blocking($this->inotify, 0);
    }
    function onOpen(ConnectionInterface $connection) {
        debug(__METHOD__);
        // Store the new connection to send messages to later
        $client = new Client([
            'server' => $this,
            'connection' => $connection,
        ]);
        $this->connections->attach($connection, $client);
    }

    function onMessage(ConnectionInterface $from, $msg) {
        debug(__METHOD__);
        debug("Message: %s", $msg);
        $msg = json_decode($msg);
        foreach($this->connections as $connection) {
            $client = $this->connections[$connection];
            if($from === $client->connection) {
                $client->onMessage($from, $msg);
            }
        }
    }

    function onClose(ConnectionInterface $connection) {
        debug(__METHOD__);
        $client = $this->connections[$connection];
        $client->onClose();
        $this->connections->detach($connection);
    }

    function onError(ConnectionInterface $connection, \Exception $e) {
        debug(__METHOD__);
        debug("An error has occurred: {$e->getMessage()}\n");
        $connection->close();
    }

    function onUpdate($timer) {
        $now = microtime(true);
        $delta = $this->lastUpdate === null ? 0.0 : $now - $this->lastUpdate;
        if($delta > 0.0) {
            //debug(__METHOD__ . ': DT=%05f, EC=%d', $delta, count($this->entities));
            $this->updateClients($now, $delta);
            //debug(__METHOD__ . ': LT=%05f', microtime(true) - $now);
        }
        $this->lastUpdate = $now;
    }

    function updateClients($now, $delta) {
        $events = inotify_read($this->inotify);
        foreach($this->connections as $connection) {
            $client = $this->connections[$connection];
            $client->onUpdate($now, $delta, $events);
        }
    }
}

$port = 33333;
$server = new Server;
$component = new HttpServer(new WsServer($server));
$loop = LoopFactory::create();
$loop->addPeriodicTimer(0.03333, [$server, 'onUpdate']);
$socket = new Reactor($loop);
$socket->listen($port, '0.0.0.0');
$server = new IoServer($component, $socket, $loop);
debug("Running!");
$server->run();
